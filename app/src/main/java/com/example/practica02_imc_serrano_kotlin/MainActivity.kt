package com.example.practica02_imc_serrano_kotlin

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var Peso: EditText
    private lateinit var Altura: EditText
    private lateinit var Imc: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun Calcular(v: View) {
        Peso = findViewById(R.id.lblPeso)
        Altura = findViewById(R.id.lblAltura)
        Imc = findViewById(R.id.lblIMC)

        if (Vali(Peso.text.toString(), Altura.text.toString())) {
            inser(Conver(Peso.text.toString()), Conver(Altura.text.toString()))
        }
    }

    fun Limpiar(v: View) {
        Peso = findViewById(R.id.lblPeso)
        Altura = findViewById(R.id.lblAltura)
        Imc = findViewById(R.id.lblIMC)

        Peso.text.clear()
        Altura.text.clear()
        Imc.text = ""
    }

    fun Cerrar(v: View) {
        finish()
    }

    private fun Vali(peso: String, altura: String): Boolean {
        if (peso.isBlank() || altura.isBlank()) {
            Toast.makeText(this, "Algun campo esta vacio.", Toast.LENGTH_SHORT).show()
        } else if (contieneLetras(peso) || contieneLetras(altura) || ValCa(peso) || ValCa(altura)) {
            Toast.makeText(this, "No introduzcas caracteres especiales o letras en los campos", Toast.LENGTH_SHORT).show()
        } else {
            return true
        }
        return false
    }

    private fun contieneLetras(cadena: String): Boolean {
        return cadena.matches(".*[a-zA-Z].*".toRegex())
    }

    private fun ValCa(cadena: String): Boolean {
        return cadena.matches(".*[^a-zA-Z0-9].*".toRegex())
    }

    private fun Conver(aop: String): Int {
        return aop.toInt()
    }

    private fun inser(peso: Int, altura: Int) {
        Imc = findViewById(R.id.lblIMC)
        val df = DecimalFormat("#.00")
        val form = peso / ((altura / 100.0) * (altura / 100.0))
        val resultado = String.format("%.2f", form)
        Imc.text = "$resultado kg/m²"
    }
}